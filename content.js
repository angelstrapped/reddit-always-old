document.addEventListener('click', function(e) {
  // If the target is not a link, then we don't want to do anything.
  var match_string = e.currentTarget.URL
  if (!match_string) {return}

  window.location.href = match_string.replace(/^https:\/\/www\.reddit\.com\//, 'https://old.reddit.com/')
})
